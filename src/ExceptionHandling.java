import java.sql.SQLOutput;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Input a number: ");
        try {
            num = input.nextInt();
        }catch (Exception e) {
            System.err.println("Invalid input. This program only accepts integer inputs.");
            e.printStackTrace();
        }
        System.out.println("The number you entered is: " + num);
    }


}

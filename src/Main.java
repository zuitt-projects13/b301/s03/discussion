
public class Main {
    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            System.out.println("Current count " + i);
        }

        int[] hundreds = { 100,200,300,400,500};
        for(int hundred : hundreds) {
            System.out.println(hundred);
        }

        // While
        int i= 0;

        while (i<10) {
            System.out.println("Current count: " + i);
            i++;
        }

        // Do While - Runs atleast once
        int y = 0;
        do {
            System.out.println("Y value: " + y);
            y++;
        } while(y <5);
    }
}